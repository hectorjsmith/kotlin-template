package org.hsmith.sample

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class SampleTest {
    @Test
    fun `GIVEN sample test WHEN run THEN test passes`() {
        // Assemble
        val variable1 = 15
        val variable2 = 25
        val expectedSum = 40

        // Act
        val sum = variable1 + variable2

        // Assert
        assertEquals(expectedSum, sum, "Sum should match expected")
    }
}
