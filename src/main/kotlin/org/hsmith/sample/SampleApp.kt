package org.hsmith.sample

fun main() {
    val app = SampleApp()
    app.run()
}

class SampleApp {
    fun run() {
        println("Hello world")
    }
}
