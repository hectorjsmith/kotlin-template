# Kotlin Template

Template for new Kotlin projects using Gradle. Includes Gitlab CI/CD configuration, JUnit5 tests, and Ktlint.

----

- Link to original template project: [kotlin-template](https://gitlab.com/hectorjsmith/kotlin-template)
- Link to project Wiki: [kotlin-template/wiki](https://gitlab.com/hectorjsmith/kotlin-template/-/wikis/home)
